### Due: 20.09.19 (23:59 CET)

# AutoML_SS19_project

Repository with the baseline code for the AutoML SS19 final project (https://ilias.uni-freiburg.de/ilias.php?ref_id=1264546&cmdClass=ilrepositorygui&cmdNode=vs&baseClass=ilrepositorygui)

To run the final version of the code, run the bash file darts_run.sh