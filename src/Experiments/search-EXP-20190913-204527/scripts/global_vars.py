import torch

opti_dict = {'adam': torch.optim.Adam,
                'adad': torch.optim.Adadelta,
                'sgd': torch.optim.SGD}
data_dir = '../data'
working_dir = 'working_dir'