import torch
import torch.utils.data
import torch.nn as nn
import torch.nn.functional as F

import torchvision
import torchvision.transforms as transforms

import ConfigSpace as CS
import ConfigSpace.hyperparameters as CSH

from hpbandster.core.worker import Worker
import hpbandster.core.result as hpres
import hpbandster.core.nameserver as hpns
from hpbandster.optimizers import BOHB
import logging

from main import main
import global_vars
from global_vars import opti_dict

import os
import pickle


class BOHBWorker(Worker):
    def __init__(self, **kwargs):
            super().__init__(**kwargs)

    def compute(self, config, budget, *args, **kwargs):
            validation_accuracy = main(
                        config,
                        data_dir=global_vars.data_dir,
                        num_epochs=int(budget),
                        batch_size=config['batch_size'],
                        learning_rate=config['lr'],
                        train_criterion=torch.nn.CrossEntropyLoss,
                        model_optimizer=opti_dict[config['optimizer']],
                        data_augmentations=None,  # Not set in this example
                        save_model_str=None
                )
            
            return ({
                    'loss': 100-validation_accuracy, # remember: HpBandSter always minimizes!
                    'info': {'score': validation_accuracy}})


    @staticmethod
    def get_configspace():

            cs = CS.ConfigurationSpace()

            lr = CSH.UniformFloatHyperparameter('lr', lower=1e-4, upper=1e-1, default_value='1e-2', log=True)
            
            optimizer = CSH.CategoricalHyperparameter('optimizer', opti_dict.keys())

            sgd_momentum = CSH.UniformFloatHyperparameter('sgd_momentum', lower=0.0, upper=0.99, default_value=0.9, log=False)

            cs.add_hyperparameters([lr, optimizer, sgd_momentum])

            cond = CS.EqualsCondition(sgd_momentum, optimizer, 'sgd')
            cs.add_condition(cond)

            n_layers = CSH.UniformIntegerHyperparameter('n_layers', lower=1, upper=3, default_value=1)

            num_conv_layers =  CSH.UniformIntegerHyperparameter('num_conv_layers', lower=1, upper=3, default_value=2)

            num_filters_1 = CSH.UniformIntegerHyperparameter('num_filters_1', lower=4, upper=64, default_value=16, log=True)
            num_filters_2 = CSH.UniformIntegerHyperparameter('num_filters_2', lower=4, upper=64, default_value=16, log=True)
            num_filters_3 = CSH.UniformIntegerHyperparameter('num_filters_3', lower=4, upper=64, default_value=16, log=True)

            kernel_size =  CSH.UniformIntegerHyperparameter('kernel_size', lower=2, upper=5, default_value=3)

            cs.add_hyperparameters([n_layers, num_conv_layers, num_filters_1, num_filters_2, num_filters_3, kernel_size])

            cond = CS.GreaterThanCondition(num_filters_2, num_conv_layers, 1)
            cs.add_condition(cond)

            cond = CS.GreaterThanCondition(num_filters_3, num_conv_layers, 2)
            cs.add_condition(cond)


            dropout_rate = CSH.UniformFloatHyperparameter('dropout_rate', lower=0.0, upper=0.9, default_value=0.5, log=False)
            num_fc_units = CSH.UniformIntegerHyperparameter('num_fc_units', lower=8, upper=256, default_value=32, log=True) 
            batch_size = CSH.UniformIntegerHyperparameter('batch_size', lower=36, upper=256, default_value=96, log=True)
            
            cs.add_hyperparameters([dropout_rate, num_fc_units, batch_size])

            return cs

def run_bohb(exp_name, iterations=24):

        working_dir = global_vars.working_dir

        run_dir = f"{working_dir}/{exp_name}/"

        result_logger = hpres.json_result_logger(directory=run_dir, overwrite=True)

        # Start a nameserver:
        NS = hpns.NameServer(run_id=exp_name, host='127.0.0.1', port=0, working_directory=run_dir)
        ns_host, ns_port = NS.start()

        # Start local worker
        w = BOHBWorker(run_id=exp_name, host='127.0.0.1', nameserver=ns_host, nameserver_port=ns_port, timeout=120)
        w.run(background=True)

        # Run an optimizer
        bohb = BOHB(  configspace = w.get_configspace(),
                run_id = exp_name,
                host='127.0.0.1',
                nameserver=ns_host,
                nameserver_port=ns_port,
                result_logger=result_logger,
                min_budget=2, max_budget=8, 
                min_points_in_model=13
                )
        res = bohb.run(n_iterations=iterations)

        # store results
        with open(os.path.join(run_dir, 'results.pkl'), 'wb') as fh:
                pickle.dump(res, fh)

        # get all executed runs
        all_runs = res.get_all_runs()

        f = open( run_dir + '/summary.txt', 'w' )
        f.write( f"{all_runs}" )
        f.close()
        # get the 'dict' that translates config ids to the actual configurations
        id2conf = res.get_id2config_mapping()

        # Here is how you get the incumbent (best configuration)
        inc_id = res.get_incumbent_id()

        # let's grab the run on the highest budget 
        inc_runs = res.get_runs_by_id(inc_id)
        inc_run = inc_runs[-1]

        # We have access to all information: the config, the loss observed during
        #optimization, and all the additional information
        inc_loss = inc_run.loss
        inc_config = id2conf[inc_id]['config']
        score = inc_run.info['score']

        print('Best found configuration:')
        print(inc_config)
        print("It achieved score of: ", score)

        # shutdown
        bohb.shutdown(shutdown_workers=True)
        NS.shutdown()


run_bohb('BOHB_24iter_KM')