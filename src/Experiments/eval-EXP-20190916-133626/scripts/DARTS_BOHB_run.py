import torch
import torch.utils.data
import torch.nn as nn
import torch.nn.functional as F

import torchvision
import torchvision.transforms as transforms

import ConfigSpace as CS
import ConfigSpace.hyperparameters as CSH

from hpbandster.core.worker import Worker
import hpbandster.core.result as hpres
import hpbandster.core.nameserver as hpns
from hpbandster.optimizers import BOHB
import logging

import global_vars
from global_vars import opti_dict

import os
import pickle

import sys
import time
import glob
import numpy as np
import utils
import argparse
import genotypes
import torch.backends.cudnn as cudnn
from datasets import K49, KMNIST
import torchvision.transforms as transforms

from torch.autograd import Variable
from model import NetworkCIFAR as Network

parser = argparse.ArgumentParser("It's K")
parser.add_argument('--data', type=str, default='../data', help='location of the data corpus')
parser.add_argument('--report_freq', type=float, default=50, help='report frequency')
parser.add_argument('--gpu', type=str, default='cuda:0', help='gpu device id')
parser.add_argument('--init_channels', type=int, default=16, help='num of init channels')
parser.add_argument('--layers', type=int, default=4, help='total number of layers')
parser.add_argument('--model_path', type=str, default='saved_models', help='path to save the model')
parser.add_argument('--auxiliary', action='store_true', default=False, help='use auxiliary tower')
parser.add_argument('--auxiliary_weight', type=float, default=0.4, help='weight for auxiliary loss')
parser.add_argument('--cutout', action='store_true', default=False, help='use cutout')
parser.add_argument('--cutout_length', type=int, default=16, help='cutout length')
parser.add_argument('--drop_path_prob', type=float, default=0.2, help='drop path probability')
parser.add_argument('--train_portion', type=float, default=0.5, help='portion of training data')
parser.add_argument('--save', type=str, default='EXP', help='experiment name')
parser.add_argument('--seed', type=int, default=0, help='random seed')
parser.add_argument('--arch', type=str, default='DARTS', help='which architecture to use')
parser.add_argument('--grad_clip', type=float, default=5, help='gradient clipping')
args = parser.parse_args()

args.save = 'eval-{}-{}'.format(args.save, time.strftime("%Y%m%d-%H%M%S"))
utils.create_exp_dir(args.save, scripts_to_save=glob.glob('*.py'))

log_format = '%(asctime)s %(message)s'
logging.basicConfig(stream=sys.stdout, level=logging.INFO,
    format=log_format, datefmt='%m/%d %I:%M:%S %p')
fh = logging.FileHandler(os.path.join(args.save, 'log.txt'))
fh.setFormatter(logging.Formatter(log_format))
logging.getLogger().addHandler(fh)

class BOHBWorker(Worker):
    def __init__(self, **kwargs):
            super().__init__(**kwargs)

    def compute(self, config, budget, *args, **kwargs):
            validation_accuracy, test_accuracy = darts_main(
                    epochs=budget,
                    batch_size=config['batch_size'],
                    learning_rate=config['lr'],
                    model_optimizer=opti_dict[config['optimizer']])
            
            return ({
                    'loss': 100-validation_accuracy, # remember: HpBandSter always minimizes!
                    'info': {'valid_score': validation_accuracy,
                             'test_score': test_accuracy}})


    @staticmethod
    def get_configspace():
            cs = CS.ConfigurationSpace()

            lr = CSH.UniformFloatHyperparameter('lr', lower=1e-4, upper=1e-1, default_value='1e-2', log=True)

            batch_size = CSH.UniformIntegerHyperparameter('batch_size', lower=36, upper=256, default_value=96, log=True)
            
            optimizer = CSH.CategoricalHyperparameter('optimizer', opti_dict.keys())

            sgd_momentum = CSH.UniformFloatHyperparameter('sgd_momentum', lower=0.0, upper=0.99, default_value=0.9, log=False)

            cs.add_hyperparameters([lr, optimizer, sgd_momentum, batch_size])

            cond = CS.EqualsCondition(sgd_momentum, optimizer, 'sgd')
            cs.add_condition(cond)

            return cs

def darts_main(epochs, batch_size, learning_rate, model_optimizer):
        if not torch.cuda.is_available():
                logging.info('no gpu device available')
                sys.exit(1)

        np.random.seed(args.seed)
        torch.cuda.set_device(args.gpu)
        cudnn.benchmark = True
        torch.manual_seed(args.seed)
        cudnn.enabled=True
        torch.cuda.manual_seed(args.seed)
        logging.info('gpu device = %s' % args.gpu)
        logging.info("args = %s", args)

        data_augmentations = None
        data_augmentations = transforms.ToTensor()

        train_dataset = K49(args.data, True, data_augmentations)
        test_dataset = K49(args.data, False, data_augmentations)

        #train_dataset = KMNIST(args.data, True, data_augmentations)
        #test_dataset = KMNIST(args.data, False, data_augmentations)

        num_classes=train_dataset.n_classes

        num_train = len(train_dataset.labels)
        indices = list(range(num_train))
        split = int(np.floor(args.train_portion * num_train))

        genotype = eval("genotypes.%s" % args.arch)
        model = Network(args.init_channels, num_classes, args.layers, args.auxiliary, genotype)
        model = model.cuda()

        logging.info("param size = %fMB", utils.count_parameters_in_MB(model))

        criterion = nn.CrossEntropyLoss()
        criterion = criterion.cuda()
        optimizer = model_optimizer(model.parameters(), lr=float(learning_rate))

        train_queue = torch.utils.data.DataLoader(
                train_dataset,
                batch_size=batch_size,
                sampler=torch.utils.data.sampler.SubsetRandomSampler(indices[:split]),
                pin_memory=True,
                num_workers=2)

        valid_queue = torch.utils.data.DataLoader(
                train_dataset,
                batch_size=batch_size,
                sampler=torch.utils.data.sampler.SubsetRandomSampler(indices[split:num_train]),
                pin_memory=True,
                num_workers=2)

        test_loader = torch.utils.data.DataLoader(
                dataset=test_dataset,
                batch_size=batch_size,
                shuffle=False)

        scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, float(epochs))

        for epoch in range(int(epochs)):
                logging.info('epoch %d lr %e', epoch, scheduler.get_lr()[0])
                model.drop_path_prob = args.drop_path_prob * epoch / epochs

                train_acc, train_obj = train(train_queue, model, criterion, optimizer)
                logging.info('train_acc %f', train_acc)

                valid_acc, valid_obj = infer(valid_queue, model, criterion)
                logging.info('valid_acc %f', valid_acc)

                scheduler.step()

                utils.save(model, os.path.join(args.save, 'weights.pt'))

        test_acc, test_obj = infer(test_loader, model, criterion)
        logging.info('test_acc %f', test_acc)

        return valid_acc, test_acc


def train(train_queue, model, criterion, optimizer):
        objs = utils.AvgrageMeter()
        top1 = utils.AvgrageMeter()
        top5 = utils.AvgrageMeter()
        model.train()

        for step, (input, target) in enumerate(train_queue):
                input = Variable(input).cuda()
                target = Variable(target).cuda(async=True)

                optimizer.zero_grad()
                logits, logits_aux = model(input)
                loss = criterion(logits, target)
                if args.auxiliary:
                        loss_aux = criterion(logits_aux, target)
                        loss += args.auxiliary_weight*loss_aux
                loss.backward()
                nn.utils.clip_grad_norm(model.parameters(), args.grad_clip)
                optimizer.step()

                prec1, prec5 = utils.accuracy(logits, target, topk=(1, 5))
                n = input.size(0)
                objs.update(loss.data.item(), n)
                top1.update(prec1.data.item(), n)
                top5.update(prec5.data.item(), n)

                if step % args.report_freq == 0:
                        logging.info('train %03d %e %f %f', step, objs.avg, top1.avg, top5.avg)

        return top1.avg, objs.avg


def infer(valid_queue, model, criterion):
        objs = utils.AvgrageMeter()
        top1 = utils.AvgrageMeter()
        top5 = utils.AvgrageMeter()
        model.eval()

        for step, (input, target) in enumerate(valid_queue):
                input = Variable(input, volatile=True).cuda()
                target = Variable(target, volatile=True).cuda(async=True)

                logits, _ = model(input)
                loss = criterion(logits, target)

                prec1, prec5 = utils.accuracy(logits, target, topk=(1, 5))
                n = input.size(0)
                objs.update(loss.data.item(), n)
                top1.update(prec1.data.item(), n)
                top5.update(prec5.data.item(), n)

                if step % args.report_freq == 0:
                        logging.info('valid %03d %e %f %f', step, objs.avg, top1.avg, top5.avg)

        return top1.avg, objs.avg

def run_bohb(exp_name, iterations=20):

        working_dir = global_vars.working_dir

        run_dir = f"{working_dir}/{exp_name}/"

        result_logger = hpres.json_result_logger(directory=run_dir, overwrite=True)

        # Start a nameserver:
        NS = hpns.NameServer(run_id=exp_name, host='127.0.0.1', port=0, working_directory=run_dir)
        ns_host, ns_port = NS.start()

        # Start local worker
        w = BOHBWorker(run_id=exp_name, host='127.0.0.1', nameserver=ns_host, nameserver_port=ns_port, timeout=120)
        w.run(background=True)

        # Run an optimizer
        bohb = BOHB(  configspace = w.get_configspace(),
                run_id = exp_name,
                host='127.0.0.1',
                nameserver=ns_host,
                nameserver_port=ns_port,
                result_logger=result_logger,
                min_budget=2, max_budget=8, 
                min_points_in_model=8
                )
        res = bohb.run(n_iterations=iterations)

        # store results
        with open(os.path.join(run_dir, 'results.pkl'), 'wb') as fh:
                pickle.dump(res, fh)

        # get all executed runs
        all_runs = res.get_all_runs()

        f = open( run_dir + '/summary.txt', 'w' )
        f.write( f"{all_runs}" )
        f.close()
        # get the 'dict' that translates config ids to the actual configurations
        id2conf = res.get_id2config_mapping()

        # Here is how you get the incumbent (best configuration)
        inc_id = res.get_incumbent_id()

        # let's grab the run on the highest budget 
        inc_runs = res.get_runs_by_id(inc_id)
        inc_run = inc_runs[-1]

        # We have access to all information: the config, the loss observed during
        #optimization, and all the additional information
        inc_loss = inc_run.loss
        inc_config = id2conf[inc_id]['config']
        valid_score = inc_run.info['valid_score']
        test_score = inc_run.info['test_score']

        print('Best found configuration:')
        print(inc_config)
        print("It achieved valid_score of: ", valid_score)
        print("While it's test_score is: ", test_score)

        # shutdown
        bohb.shutdown(shutdown_workers=True)
        NS.shutdown()


run_bohb('DARTS_BOHB_20iter_K49')